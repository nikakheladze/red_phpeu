<?php
    include "functions.php";
    $cap = get_random();
    // $result = (isset($_POST['u_captcha']) && ($_POST['u_captcha']==$_POST['captcha'])) ? "Correct" : "Is Not Correct";

    if(isset($_POST['u_captcha'])){
        $result = ($_POST['u_captcha']==$_POST['captcha']) ? "Correct" : "Is Not Correct";
    }else{
        $result = "";
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task3-3</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <div class="captcha">
            <?=$cap?>
        </div>
        <form method="post">
            <input type="text" name="u_captcha">
            <input type="hidden" value="<?=$cap?>" name="captcha">
            <button>Check Captcha</button>
        </form>
        <div class="captcha">
           <?=$result?>
        </div>
    </div>
</body>
</html>