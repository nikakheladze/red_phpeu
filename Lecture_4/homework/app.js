function showTime() {
    var date = new Date()
    var h = date.getHours()
    var m = date.getMinutes()
    var s = date.getSeconds()
    var session = "PM"

    if(h > 12) {
        h -= 12;
        session = "AM"
    }

    if(h < 10) {
        h = "0" + h
    }

    if(m < 10) {
        m = "0" + m
    }

    if(s < 10) {
        s = "0" + s
    }

    var time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("myClock").textContent = time

    setTimeout(showTime)
}

showTime()

// function timer_f() {

//     let count = 0;

//     document.getElementById("start").onclick = function() {
//         count += 1;
//         document.getElementById("countLabel").innerHTML = count
//     }

//     document.getElementById("stop").onclick = function() {
//         count = 0;
//         document.getElementById("countLabel").innerHTML = count
//     }

// }

//! 5.2
function Timer(){
    var count = 0;
    document.getElementById('start').onclick = function(){
        count += 1;
        document.getElementById("GetTimer").innerHTML = count
    }
}
Timer()

