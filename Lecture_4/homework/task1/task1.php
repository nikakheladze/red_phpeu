<?php
    include "validations.php";
    $cap = get_random();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <form action="" method='post'>
        <h3>სარეგისტრაციო ფორმა</h3>
        <br>
        <input type="text" name="email"> - Email
        <h2><div><?=$errormsg?></div></h2>
        <br><br>
        <input type="password" name="password"> - Password
        <h2><div><?=$errormsg_2?></div></h2>
        <br><br>
        <input type="password" name="rep-password"> - Repeat Password
        <h2><div><?=$errormsg_3?></div></h2>
        <br><br>
        <h3><div><?=$cap?></div></h3>
        <br><br>
        <input type="text" name="u_captcha">
        <h2><div><?=$result?></div></h2>
        <input type="hidden" value="<?=$cap?>" name="captcha">
        <br><br>
        <button>Submit</button>
        </form>
    </div>
</body>
</html>