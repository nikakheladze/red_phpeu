
function setTimer(){
    let count = 0;

    const interval = setInterval(function(){
        count++;

        if(count < 10) {
            count = "0" + count;
        }

        document.getElementById('countLabel').innerHTML = count;

        const button = document.getElementById('stop');

        button.addEventListener('click', function() {
            clearInterval(interval);
        });

    }, 1000);

}