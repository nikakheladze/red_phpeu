<?php

$errors = [];

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty($_POST['sakheli'])) {
        $errors[] = "სახელი ცარიელია";
    } else {
        $sakheli = ($_POST['sakheli']);
        if(strlen($sakheli) < 2 || strlen($sakheli) > 20) {
            $errors[] = "სახელი 2-დან 20 სიმბოლომდე უნდა იყოს";
        }
    }

    if(empty($_POST['gvari'])) {
        $errors[] = "გვარი ცარიელია";
    } else {
        $gvari = ($_POST['gvari']);
        if(strlen($gvari) < 3 || strlen($gvari) > 50) {
            $errors[] = "გავრი 3-დან 50 სიმბოლომდე უნდა იყოს";
        }
    }

    if(empty($_POST['dab-tarigi'])) {
        $errors[] = "დაბ. თარიღი არ არის არჩეული";
    }

    if(empty($_POST['personal-number'])) {
        $errors[] = "პირადი ნომერი ცარიელია";
    } else {
        $personalNumber = ($_POST['personal-number']);
        if(!preg_match('/^\d{11}$/', $personalNumber)) {
            $errors[] = "პირადი ნომერი უნდა იყოს 11 ციფრი";
        }
    }

    $misamarti = ($_POST['misamarti']);
    if(strlen($misamarti) > 70) {
        $errors[] = "მისამართი 70 სიმბოლომდე უნდა იყოს";
    }

    $registrationDate = date("Y-m-d H:i:s");

    $mobile = ($_POST['mobile']);
    if(!empty($mobile) && !preg_match('/^\d{9}$/', $mobile)) {
        $errors[] = "მობილური ნომერი უნდა იყოს 9 ციფრი";
    }

    if(!empty($errors)) {
        echo "<h2>არავალიდური მონაცემები:</h2>";
        echo "<ul>";
        foreach($errors as $error) {
            echo "<li>$error</li>";
        }
        echo "</ul>";
    } else {
        echo "<h2>თქვენი შეყვანილი მონაცემები:</h2>";
        echo "<table border='1' style='text-align:center'>";
        echo "<tr>
            <th>სახელი</th>
            <th>გვარი</th>
            <th>დაბ. თარიღი</th>
            <th>პირადი ნომერი</th>
            <th>მისამართი</th>
            <th>რეგისტრაციის თარიღი</th>
            <th>მობილური</th>
            <th>დამატებითი ინფორმაცია</th>
            </tr>";
        echo "<tr>
            <td>$sakheli</td>
            <td>$gvari</td>
            <td>{$_POST['dab-tarigi']}-{$_POST['dab-tarigi_2']}-{$_POST['dab-tarigi_3']}</td>
            <td>$personalNumber</td>
            <td>$misamarti</td>
            <td>$registrationDate</td>
            <td>$mobile</td>
            <td>{$_POST['damatebiti-info']}</td>
            </tr>";
        echo "</table>";    
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="info.php">
        <br><br>
        <button>შეავსე თავიდან</button>
    </form>
</body>
</html>