<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personal Infromation</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="form">
    <h2>პერსონალური ინფორმაცია</h2>
    <form action="action.php" method="post">
        სახელი : <input type="text" name="sakheli" required>
        <br><br>
        გვარი : <input type="text" name="gvari" required>
        <br><br>

        დაბ. თარიღი : <select name="dab-tarigi" required>
            <?php
                for($year = date("Y"); $year >= 1950; $year--) {
                    echo "<option value='$year'>$year</option>";
                }
            ?>
        </select>
        <select name="dab-tarigi_2" required>
                <?php
                    for ($i = 0; $i < 12;   $i++) {
                        $date_str = date('M', strtotime("+ $i months"));
                        echo "<option value=$i>".$date_str ."</option>";
                    }
                ?>
        </select>
        <select name="dab-tarigi_3" required>
            <?php
                for($i = 1; $i < 32; $i++) {
                    echo "<option value=$i>".$i."</option>";
                } 
            ?>
        </select>
        <br><br>

        პირადი ნომერი : <input type="text" name="personal-number" required>
        <br><br>
        მისამართი : <input type="text" name="misamarti" required>
        <br><br>
        მობილური : <input type="tel" name="mobile" required>
        <br><br>
        დამატებითი ინფორმაცია : <textarea name="damatebiti-info" cols="50" rows="4"></textarea>
        <br><br>
        <button>გაგზავნა</button>
    </form>
    </div>
</body>
</html>