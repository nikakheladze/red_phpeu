<?php
$formAction="";
$errors = 0;
function year(){
    $y=1950;
    $d=0;
    for($i=2023;$i>=$y;$i--){
        echo "<option>".$i."</option>";
    }

}

function month(){
    $months = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July ',
        'August',
        'September',
        'October',
        'November',
        'December',
    );

    foreach($months as $month){
        echo "<option>".$month."</option>";
    }
}

function day(){
    for($i=1;$i<=31;$i++){
        echo "<option>".$i."</option>";
    }
}
function nameValidation(){
    if(isset($_POST["name"])){
        if(strlen($_POST["name"]) >= 2 && strlen($_POST["name"]) <=20){
            $result = "Validation is correct";
        }else{
            $result="Validation is not correct";
            global $errors;
            $errors +=1;
        }
    }else{
        $result = "";
    }
    global $errors;
    echo $result;
    echo $errors;
    
}

function surnameValidation(){
    if(isset($_POST["surname"])){
        if(strlen($_POST["surname"]) >= 3 && strlen($_POST["surname"]) <=50){
            $result = "Validation is correct";
        }else{
            $result="Validation is not correct";
            global $errors;
            $errors+=1;
        }
    }else{
        $result = "";
    }
    global $errors;
    echo $result;
    echo $errors;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    nameValidation();
    surnameValidation();
    if ($errors == 0) {
        $formAction = "action.php";
        echo $_POST["name"];
        header("Location: $formAction");
        echo $_POST["name"];
    }
}

?>