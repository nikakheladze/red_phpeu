<?php

function generateRandomWord() {
    $words = ["apple", "banana", "cherry", "date", "fig", "grape", "kiwi", "lemon", "mango", "orange"];
    return $words[array_rand($words)];
}

function generateRandomSentence($cinadadeba) {
    $sentence = [];
    for ($i = 0; $i < $cinadadeba; $i++) {
        $sentence[] = generateRandomWord();
    }
    return (implode(' ', $sentence) . '.');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $cinadadeba = (int)$_POST['cinadadeba'];
    $sityvacinadadeba = (int)$_POST['sityva-cinadadeba'];

    $randomString = '';
    for ($i = 0; $i < $cinadadeba; $i++) {
        $randomString .= generateRandomSentence($sityvacinadadeba) . ' ';
    }
} else {
    
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Random String Generator</title>
</head>
<body>
    <h1>Random String Generator</h1>
    <form method="post">
        წინადადებების რაოდენობა : <input type="number" name="cinadadeba" value="<?= $cinadadeba ?>">
        <br>
        სიტყვების რაოდენობა წინადადებაში : <input type="number" name="sityva-cinadadeba" value="<?= $sityvacinadadeba ?>">
        <br>
        <button>დაგენერირება</button>
    </form>
    
    <h2>Random String:</h2>
    <p><?= $randomString ?></p>
</body>
</html>
