<?php
    session_start();

    $err_msg = [
        'sakheli' =>"",
        'gvari' =>""
    ];


    $old_val = [
        'sakheli' =>"",
        'gvari' =>"",
        'bool' =>FALSE
    ];

    if(isset($_POST['sakheli'])) {
        if(strlen($_POST['sakheli']) <=4) {
            $err_msg['sakheli'] = "is not valid";
        } else {
            $old_val['sakheli'] = $_POST['sakheli'];
        }
    }

    if(isset($_POST['gvari'])) {
        if(strlen($_POST['gvari']) <=4) {
            $err_msg['gvari'] = "is not valid";
            $err_msg['bool'] = TRUE;
        } else {
            $old_val['gvari'] = $_POST['gvari'];
            $err_msg['bool'] = FALSE;
        }
    }

    if(isset($_POST['send']) && $err_msg['sakheli']== "" && $err_msg['gvari']== "") {
        $_SESSION['sakheli'] = $_POST['sakheli'];
        header("location:action.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <input type="text" name="sakheli" value="<?=$old_val['sakheli']?>"> - <?=$err_msg['sakheli']?>
        <br><br>
        <input type="text" name="gvari" value="<?=$old_val['gvari']?>"> - <?=$err_msg['gvari']?>
        <br><br>
        <button name='send'>Send Info</button>
    </form>
</body>
</html>