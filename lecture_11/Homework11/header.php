<?php
    session_start();
?>
<header>
    <link rel="stylesheet" href="style.css">
    <p>DASAKMEBA.GE</p>
    <h1 class="your-class-name">Welcome To Admin Panel - <?php echo $_SESSION['AdminLoginId']?></h1>
</header>
<form method="POST">
    <button class="admin-logout" name="Logout">LOG OUT</button>
</form>

<?php
    if(isset($_POST['Logout'])) {
        session_destroy();
        header("location: index.php");
    }
?>