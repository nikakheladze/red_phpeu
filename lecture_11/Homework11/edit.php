<?php
    include "Connection.php";

    if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['id'])) {
        $id = $_GET['id'];
        
        $select_query = "SELECT * FROM jobs WHERE id = $id";
        $result = mysqli_query($con, $select_query);
        $data = mysqli_fetch_assoc($result);
    } else {
        header("Location: index.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Data</title>
</head>
<body>
    <h1>Edit Data</h1>
    <form action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $id; ?>">

        <label for="field1">VIP განცხადებები:</label>
        <input type="text" name="field1" value="<?php echo $data['VIP განცხადებები']; ?>">

        <label for="field2">მომწოდებელი:</label>
        <input type="text" name="field2" value="<?php echo $data['მომწოდებელი']; ?>">

        <label for="field3">გამოქვეყნდა:</label>
        <input type="text" name="field3" value="<?php echo $data['გამოქვეყნდა']; ?>">

        <label for="field4">ბოლო ვადა:</label>
        <input type="text" name="field4" value="<?php echo $data['ბოლო ვადა']; ?>">


        <button type="submit">Update</button>
    </form>
</body>
</html>
