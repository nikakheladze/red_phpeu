-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2024 at 07:04 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobs.ge`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `ID` int(50) NOT NULL,
  `VIP განცხადებები` varchar(50) NOT NULL,
  `მომწოდებელი` varchar(50) NOT NULL,
  `გამოქვეყნდა` varchar(50) NOT NULL,
  `ბოლო ვადა` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`ID`, `VIP განცხადებები`, `მომწოდებელი`, `გამოქვეყნდა`, `ბოლო ვადა`) VALUES
(10, 'გაყიდვების მენეჯერი', 'პროკრედიტ ბანკი', '10 იანვარი', '31 იანვარი'),
(11, 'უძრავი ქონების აგენტი/ბროკერი', 'თბილისი ბროკერ', '10 იანვარი', '10 თებერვალი'),
(12, 'კორპორაციული გაყიდვების მენეჯერი', 'არდი დაზღვევა', '10 იანვარი', '10 თებერვალი'),
(13, 'უძრავი ქონების აგენტი', 'ნიქსი', '10 იანვარი', '10 თებერვალი'),
(14, 'გაყიდვების მენეჯერი, ოფისის მენეჯერი ', 'პრომო ჰაუსი', '08 იანვარი', '08 თებერვალი'),
(15, 'CNC დანადგარის ოპერატორი', 'ავეჯის ფაბრიკა ვუდმასტერი', '04 იანვარი', '04 თებერვალი');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
