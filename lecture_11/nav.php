<?php
    $nav_select = "SELECT * FROM collections";
    $result = mysqli_query($connect, $nav_select);
    // print_r($result);
    $navs = mysqli_fetch_all($result);
    // echo "<pre>";
    // print_r($navs);
    // echo "</pre>";
    $currentCategory = isset($_GET['category']) ? $_GET['category'] : null;

?>
<nav>
    <ul>
        <li><a href="index.php">HOME</a></li>
        <?php
            foreach($navs as $menu){
        ?>
            <li><a href="?category=<?=$menu[0]?>"><?=$menu[1]?></a></li>
        <?php
            }
        ?>
        <?php
        ?>
    </ul>
</nav>
<?php
switch ($currentCategory) {
    case 1:
        include_once "back/content_1.php";
        break;
    case 2:
        include_once "back/content_2.php";
        break;
    case 3:
        include_once "back/content_3.php";
        break;
    case 4:
        include_once "back/content_4.php";
        break;
    case 5:
        include_once "back/content_5.php";
        break;
    case 6:
        include_once "back/content_6.php";
        break;
    case 7:
        include_once "back/content_7.php";
        break;
    case 8:
        include_once "back/content_8.php";
        break;
    case 9:
        include_once "back/content_9.php";
        break;     
    case 10:
        include_once "back/content_10.php";
        break;                           
    default :
       null;
       break; 
 }
?>