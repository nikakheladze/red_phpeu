<?php
    function matrix() {
        $matrix = array();

        for($i = 0; $i < 10; $i++){
            for($j = 0; $j < 10; $j++) {
                $matrix[$i][$j] = rand(10, 99);
            }
        }

        echo "<table border='2'>";
        foreach($matrix as $row) {
            echo "<tr>";
            foreach($row as $element) {
                echo "<td>$element</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    matrix()
?>
