<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="" method="post">
    <input type="number" name="M"> - მატრიცა - დან
    <input type="number" name="N"> - მატრიცა - მდე
    <br><br>
    <input type="number" name="a"> - შუალედი - დან
    <input type="number" name="b"> - შუალედი - მდე
    <br><br>
    <button>Submit</button>
</form>
<?php
    function matrix() {

        $M = $_POST["M"];
        $N = $_POST["N"];
        $a = $_POST["a"];
        $b = $_POST["b"];

        $matrix = array();

        for($i = 0; $i < $M; $i++) {
            for($j = 0; $j < $N; $j++) {
                $matrix[$i][$j] = rand($a, $b);
            }
        }

        echo "<table border='0' style='text-align:center'>";
        foreach($matrix as $row) {
            echo "<tr>";
            foreach($row as $element) {
                echo "<td>$element</td>";
            }
            echo "</tr>";
        }
        echo "</table>";

    }
    matrix()
?>
</body>
</html>