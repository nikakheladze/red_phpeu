<?php
session_start();

// Get the security code entered by the user
$user_entered_code = $_POST['security_code'];

// Get the stored security code from the session
$stored_security_code = $_SESSION['security_code'];
echo $stored_security_code;

// Verify the user-entered code
if ($user_entered_code == $stored_security_code) {
    echo "Security code is valid. Access granted!";
} else {
    echo "Invalid security code. Access denied!";
}

// Clear the session variable after verification (optional)
unset($_SESSION['security_code']);
?>