<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Security Code Verification</title>
</head>

<body>
    <h2>Enter the Security Code:</h2>
    <form action="task3_2.php" method="post">
        <label for="security_code">Security Code:</label>
        <input type="text" id="security_code" name="security_code" maxlength="5" required>
        <button type="submit">Submit</button>
    </form>
</body>

</html>