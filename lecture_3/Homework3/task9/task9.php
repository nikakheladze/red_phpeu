<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        შეიყვანეთ პაროლი - <input type="text" name="password">
        <br><br>
        <button>Submit</button>
    </form>
<?php
    $password = $_POST['password'];

    if (strlen($password) < 6) {
        echo "პაროლი არის სუსტი (შეიცავს 6 ზე ნაკლებ სიმბოლოს).";
    } elseif (strlen($password) < 10) {
        echo "პაროლი არის საშუალო (შეიცავს 6 დან 10 მდე სიმბოლოს).";
    } else {
        echo "პაროლი არის ძლიერი (10 ან მეტი სიმბოლო).";
    }
?>
</body>
</html>