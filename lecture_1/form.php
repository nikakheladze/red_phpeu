<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Form</title>
</head>
<body>
    <form action="get_method.php" method="get" style="padding-left: 100px">
    <h2>GET</h2>
        Name - <input type="text" name="sakheli">
        <br>
        Lastname - <input type="text" name="gvari">
        <br>
        Degree - <input type="text" name="khariskhi">
        <br>
        <button>Send Information</button>
    </form>
    <hr><hr>
    <form action="post_method.php" method="post" style="padding-left: 100px">
    <h2>POST</h2>
        Name - <input type="text" name="sakheli">
        <br>
        Lastname - <input type="text" name="gvari">
        <br>
        Degree - <input type="text" name="khariskhi">
        <br>
        <button>Send Information</button>
    </form>
</body>
</html>