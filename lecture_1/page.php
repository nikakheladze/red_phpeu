<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
</head>
<body>
    <?php
        echo "<h2> Hello I am a PHP Code</h2>";
        $n = 20;
        $n1 = 34.98;
        $s = "Hello PHP";
        var_dump($n);
        echo "<hr>";
        var_dump($n1);
        echo "<hr>";
        var_dump($s);
        echo "<hr>";
        $m = ["Nika", "Kheladze", 19, "kaibichi"];
        print_r($m);
        $m_a = [
            'name' => "Nika",
            'lastname' => "Kheladze",
            'age' => "19",
            'degre' => "kaibichi"
        ];
        echo "<hr>";
        print_r($m_a);
        echo "<hr>";
        echo $m[1];
        echo "<hr>";
        echo $m_a['lastname'];
    ?>
</body>
</html>