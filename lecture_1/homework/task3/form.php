<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TEST FOR STUDENTS</title>
</head>
<body>
    <h1>TEST FOR STUDENTS</h1>

    <?php
    $questions = [
        [
            'type' => 'test',
            'question' => 'დაასახელეთ საქართველოს დედაქალაქი',
            'options' => ['a' => 'თბილისი', 'b' => 'ქუთაისი', 'c' => 'სამტრედია', 'd' => 'მცხეთა'],
            'correct' => 'a',
        ],
        [
            'type' => 'test',
            'question' => 'რა ნომრიანი მაისურით თამაშობს ხვიჩა კვარაცხელია ნაპოლიში?',
            'options' => ['a' => '33', 'b' => '7', 'c' => '10', 'd' => '77'],
            'correct' => 'd',
        ],
        [
            'type' => 'test',
            'question' => 'რომელ წელს მოხდა დიდგორის ბრძოლა ?',
            'options' => ['a' => '2023', 'b' => '1121', 'c' => '1200', 'd' => '1795'],
            'correct' => 'b',
        ],
        [
            'type' => 'open',
            'question' => 'რომელია მსოფლიოში ყველაზე მაღალი მწვერვალი?',
            'correct' => 'ევერესტი',
        ],
        [
            'type' => 'open',
            'question' => 'რომელი საქართველოში ბიჭის ყველაზე გავრცელებული სახელი ?',
            'correct' => 'გიორგი',
        ],
    ];

    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $score = 0;

        foreach ($questions as $key => $question) {
            $userAnswer = $_POST['answer'][$key];
            
            if ($question['type'] === 'test' && isset($question['correct'])) {
                if ($userAnswer === $question['correct']) {
                    $score++;
                }
            } elseif ($question['type'] === 'open' && isset($question['correct'])) {
                $correctAnswer = strtolower($question['correct']);
                $userAnswer = strtolower($userAnswer);

                if ($userAnswer === $correctAnswer) {
                    $score++;
                }
            }
        }

        echo "<h2>Your Score: $score / " . count($questions) . "</h2>";
    }
    ?>

<form method="POST">
        <?php foreach ($questions as $key => $question) 
        { 
        ?>
            <p>
                <strong>Question <?php 
                echo $key + 1; 
                ?>
                :</strong>
                <br>
                <?php 
                echo $question['question']; 
                ?>
                <br>

                <?php if ($question['type'] === 'test') 
                { 
                ?>
                    <?php foreach ($question['options'] as $optionKey => $option) { ?>
                        <label>
                            <input type="radio" name="answer[<?php echo $key; ?>]" value="<?php echo $optionKey; ?>">
                            <?php echo $option; ?>
                        </label><br>
                    <?php } ?>
                <?php } elseif ($question['type'] === 'open') { ?>
                    <input type="text" name="answer[<?php echo $key; ?>]" required><br>
                <?php } ?>
            </p>
        <?php } ?>

        <input type="submit" value="Submit">
    </form>
</body>
</html>