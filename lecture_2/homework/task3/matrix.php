<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4x4 Matrix</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>4x4 Matrix</h1>
    <?php
        $matrix = array(
            array(0, 0, 0, 0),
            array(0, 10, 20, 0),
            array(0, 30, 40, 0),
            array(0, 0, 0, 0)
        );


        for($i=0; $i<= 3; $i++) {
            for ($j = 0; $j <= 3; $j++) {
                $matrix[$i][$j] = rand(10, 100);
            }
        }

        // echo "<h2>Matrix:</h2>";
        // echo "<pre>";
        // print_r($matrix);

        $sum = 0;
        $multiplied = 1;
        echo "<table border='2' width='300'px height='300'px>";
        foreach($matrix as $row) {
            echo "<tr>";
            foreach($row as $element) {
                echo "<td>$element</td>";
                $sum += $element;
                $multiplied *= $element;
                
            }
            echo "</tr>";
            
        }   
        echo "</table>";
        echo "მატრიცის ელემენტების ჯამი : ".$sum;
        echo "<br>";
        echo "მატრიცის ელემენტების ნამრავლი : ".$multiplied;
        echo "<br>";
        echo "მატრიცის ელემენტების საშ. არითმეტიკული : ".$sum / 16;

        echo "<br>";
        echo "<br>";


        $f_element = $matrix[0][1];
        $s_element = $matrix[1][2];
        $t_element = $matrix[2][3];

        echo "<h3>მთავარი დიაგონალის ზემოთ მდგომი ელემენტები</h3>";

        echo "<table border='2' width='200'px height='100'px>";
        echo "<td width='40'px></td>";
        echo "<td>".$f_element."</td>";
        echo "<td>".$s_element."</td>";
        echo "<td>".$t_element."</td>";



        echo "<h3 class='header_3'>თითოეული ელემენტის ციფრთა ჯამი</h3>";
        echo "<table class='table_3' border='2' width='200'px height='200'px>";
        foreach($matrix as $row) {
            echo "<tr>";
            foreach($row as $element) {
                $digits = str_split((string)$element);
                $digitSum = array_sum($digits);
                echo "<td>$digitSum</td>";
            }
            echo "<tr>";
        }
        echo "</table>";
        
        echo "<br>";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $x = $_POST["x_input"];
            echo "შეტანილი რიცხვი : ".$x;

            // echo "<h3>შეტანილი x - რიცხვის ჯერადი რიცხვები</h3>";

            foreach($matrix as $row) {
                foreach($row as $element) {
                    if($element % $x == 0) {
                        echo "<br>".$element;
                    } else {
                        
                    }
                } 
            }
        }
        

    ?>

    <h2 class="header_4">შეიყვანეთ - x : </h2>
    <form class="x" method="post">
        <input type="number" name="x_input">
        <input type="submit" value="Submit">
    </form>

</body>
</html>