<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>6x5 Matrix</title>
</head>
<body>
    
    <?php
        $matrix = array();

        for ($i = 0; $i < 6; $i++) {
            for ($j = 0; $j < 5; $j++) {
                $matrix[$i][$j] = $i + $j;
            }
        }
        // echo "<pre>";
        // print_r($matrix)


        echo "<table border='1' width='200'px height='200'px>";
        foreach($matrix as $row) {
            echo "<tr>";
            foreach($row as $element) {
                echo "<td>$element</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    ?>

</body>
</html>