<?php
    include "questions.php";
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    // $studentName = "";
    // $studentLastName = "";

    // // Check if student name and last name are set in POST data
    // if (isset($_POST['st_name'])) {
    //     $studentName = $_POST['st_name'];
    // }
    
    // if (isset($_POST['st_lastname'])) {
    //     $studentLastName = $_POST['st_lastname'];
    // }

    // print_r($_POST)
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grade</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="home">
        <form action="result.php" method="post">
            <h1>PHP Quiz</h1>
            <div class="student-info">
                <h2>
                <?php
                    // echo $_POST['st_name']." ".$_POST['st_lastname'];
                    // echo $studentName . " " . $studentLastName;
                    if(isset($_POST['st_name'])){
                        echo ($_POST['st_name']);
                    }
                    if(isset($_POST['st_lastname'])){
                        echo ($_POST['st_lastname']);
                    }
                
                ?>
               </h2>
            </div>
            <table>
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Point</th>
                    <th>Sum</th>
                </tr>
                <?php
                    foreach($questions as $key=>$question){
                ?>
                <tr>
                    <td><?=$question['question']?></td>
                    <td><?=$_POST['answer'][$key]?></td>
                    <td><?=$_POST['userpoint'][$key]?></td>
                    <td><input size="5" type="number"></td>
                </tr>
                <?php
                    }
                ?>
            </table>
        </form>
    </div>
</body>
</html>