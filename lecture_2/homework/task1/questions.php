<?php
    $questions = [
        ['question' => "What does PHP stand for?", 'point' => 2],
        ['question' => "PHP server scripts are surrounded by delimiters, which?", 'point' => 2],
        ['question' => "How do you write 'Hello World' in PHP", 'point' => 3],
        ['question' => "All variables in PHP start with which symbol?", 'point' => 3],
        ['question' => "What is the correct way to end a PHP statement?", 'point' => 4],

    ]
?>