<?php
    include "questions.php";
    // echo "<pre>";
    // print_r($questions);
    // echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Class Work 2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="home">
        <form action="grade.php" method="post">
            <h1>PHP Quiz</h1>
            <div class="student-info">
                <input type="text" placeholder="Student Name" name="st_name">
                <input type="text" placeholder="Student Lastname" name="st_lastname">
            </div>
            <table>
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>point</th>
                </tr>
                <?php
                    foreach($questions as $question){
                ?>
                <tr>
                    <td><?=$question['question']?></td>
                    <td><input type="text" name="answer[]"></td>
                    <td><?=$question['point']?></td>
                </tr>
                <?php
                    }
                ?>
            </table>
            <button class="send">Send</button>
        </form>
    </div>
</body>
</html>