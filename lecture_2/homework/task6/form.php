<?php
    // $s = "Who \n s Peter Griffin?\"";
    // echo $s;
    // echo "<br>";
    // echo stripslashes($s);

    $error_msg = "";
    $name = "";
    $email = "";
    $website = "";
    $comment = "";
    

    if(isset($_POST['submit'])) {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $website = $_POST['website'];
        $comment = $_POST['comment'];
        // $gender = $_POST['gender'];

        if(strlen($email)<3) {
            $error_msg = "Email is not valid!<br>";
            $email_required = TRUE;
        }
        if(strlen($name)<3) {
            $error_msg = "Name is not valid!";
            $name_required = TRUE;
        }
        if(strlen($comment)<0) {
            $comment_required = TRUE;
        }
        if(!isset($_POST['gender'])) {
            $error_msg = "Gender is not valid!";
        }
        if(!isset($name_required) and !isset($email_required)) {
            $name = "";
            $email = "";
            $website = "";
            // $website = "";
            // $comment = "";
        }


    }
?>


<div class="container">
    <h2>PHP Form Validation Example</h2>
    <p style="color: red">* required field</p>
    <div class="error"><?=$error_msg?></div>
    <form action="" method="post">
        Name : <input type="text" name="name" value="<?=$name ?>">  *
        <br><br>
        Email : <input type="text" name="email" value="<?=$email ?>">  *
        <br><br>
        Website : <input type="text" name="website" value="<?=$website ?>">
        <br><br>
        Comment : <textarea name="comment"cols="40" rows="5" value="<?=$comment ?>"></textarea>
        <br><br>
        Gender : <input type="radio" name="gender" value="Female" >Female
                <input type="radio" name="gender" value="Male">Male
                <input type="radio" name="gender" value="Other">Other *
        <br><br>        
        <button name="submit">Submit</button>
        <br><br>
        <h2>Your Input:</h2>
        <br><br>
        <?php
            if(!empty($_POST['name']) and !empty($_POST['email']) and !empty($_POST['gender'])) {
        ?>
        <div class="form">
        <table>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Website</th>
                <th>Comment</th>
                <th>Gender</th>
            </tr>
            <tr>
                <td><?=$_POST['name']?></td>
                <td><?=$_POST['email']?></td>
                <td><?=$_POST['website']?></td>
                <td><?=$_POST['comment']?></td>
                <td><?=$_POST['gender']?></td>
            </tr>
        </table>
        </div>
    </form>
    <?php
            }
    ?>
</div>