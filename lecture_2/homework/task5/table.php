<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <?php
        $cars = array(
            array("Make" => "Toyota",
                "Model" => "Corolla",
                "Color" => "White",
                "Mileage" => 24000,
                "Status" => "Sold"),
            
            array("Make" => "Toyota",
                "Model" => "Camry",
                "Color" => "Black",
                "Mileage" => 56000,
                "Status" => "Available"),
                
            array("Make" => "Honda",
                "Model" => "Accord",
                "Color" => "White",
                "Mileage" => 15000,
                "Status" => "Sold")    
        );


        echo "<table border='1' width='400'px height='200'px>";
            echo "<th>Make</th>";
            echo "<th>Model</th>";
            echo "<th>Color</th>";
            echo "<th>Mileage</th>";
            echo "<th>Status</th>";
        foreach($cars as $car) {
            echo "<tr>";
            foreach ($car as $element) {
                echo "<td>$element</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    ?>
</body>
</html>