<!-- <?php
    include "questions.php";
    echo "<pre>";
    print_r($questions);
    echo "</pre>";
?> -->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Class Work 2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="home">
        <form action="result.php" method="post"></form>
        <h1>PHP Quiz</h1>
        <div class="student-info">
            <h2>
            <?php
                echo $_POST['studentname']." ".$_POST['lastname'];
            ?>
            </h2>
        </div>
        <table>
            <tr>
                <th>Question</th>
                <th>Answer</th>
                <th>Point</th>
                <th>Grade</th>
            </tr>
            <?php
                foreach($questions as $key=>$question) {

            ?>

            <tr>
                <td><?=$question['question']?></td>
                <td><?=$_POST['answer']?></td>
                <td><?=$question['point']?></td>
            </tr>
            <?php
                }
            ?>
        </table>
        <button class="send">Send</button>
    </div>
</body>
</html>