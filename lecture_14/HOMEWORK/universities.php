<?php
    $server = "localhost";
    $user = "root";
    $password =  "";
    $db = "db";

    $connect = mysqli_connect($server, $user, $password, $db);
    mysqli_set_charset($connect, "utf8mb4"); 

    $select_data = "SELECT * FROM universities";
    $result_data = mysqli_query($connect, $select_data);
    $universities = mysqli_fetch_all($result_data);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <div class="left">
            <?php
                foreach($universities  as $items){
                    echo "<a href='?university=$items[0]'>$items[1]</a>" ;
                    echo "<hr>";
                }
            ?>
        </div>
        <div class="right">
            <?php
                if(isset($_GET['university'])){
                    $universities_id = $_GET['university'];
                    $select_full_info = "SELECT * FROM universities WHERE id='$universities_id'";
                    $result_full_info = mysqli_query($connect,  $select_full_info);
                    $full_info = mysqli_fetch_assoc($result_full_info);
                    echo $full_info['დასახელება'];
                    echo "<br><br>";
                    echo $full_info['სტუდენტების რაოდენობა'];
                    echo "<br><br>";
                    echo $full_info['რეიტინგი'];
                    echo "<br><br>";
                    echo $full_info['წელი'];
                }
            ?>
        </div>
    </div>
</body>
</html>