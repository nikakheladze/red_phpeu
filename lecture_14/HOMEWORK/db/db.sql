-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2023 at 04:01 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `ID` int(11) NOT NULL,
  `ავტორი` varchar(50) NOT NULL,
  `სათაური` varchar(50) NOT NULL,
  `ფასი` int(50) NOT NULL,
  `რაოდენობა` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`ID`, `ავტორი`, `სათაური`, `ფასი`, `რაოდენობა`) VALUES
(1, 'Suzanne Collins', 'The Hunger Games', 23, '4'),
(2, 'J.K. Rowling', 'Harry Potter and the Order of the Phoenix', 25, '10'),
(3, 'Jane Austen', 'Pride and Prejudice', 14, '7'),
(4, 'Harper Lee', 'To Kill a Mockingbird', 16, '8'),
(5, 'Markus Zusak', 'The Book Thief', 14, '2');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `ID` int(11) NOT NULL,
  `მწარმოებელი` varchar(50) NOT NULL,
  `მოდელი` varchar(50) NOT NULL,
  `წელი` int(50) NOT NULL,
  `ფასი` double(50,5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`ID`, `მწარმოებელი`, `მოდელი`, `წელი`, `ფასი`) VALUES
(1, 'BMW', 'M5 E60', 2004, 22.50000),
(2, 'Nissan', 'Skyline r34', 1999, 30.50000),
(3, 'Porsche', '911', 2001, 200.00000),
(4, 'Opel', 'Corsa', 2003, 3.20000),
(5, 'Suzuki', 'GRAND VITARA', 2002, 7.20000);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `ID` int(11) NOT NULL,
  `დასახელება` varchar(50) NOT NULL,
  `მოსახლეობა` int(50) NOT NULL,
  `საფეხბურთო კლუბის` varchar(50) NOT NULL,
  `წელი` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`ID`, `დასახელება`, `მოსახლეობა`, `საფეხბურთო კლუბის`, `წელი`) VALUES
(1, 'ხაშური', 31000, 'ხაშურის ივერია', 1998),
(2, 'კავთისხევი', 7000, 'სკ. კავთისხევი', 1810),
(3, 'წყალტუბო', 19500, 'წყალტუბოს სამგურალი', 1912),
(4, 'ნედლათი', 1500, 'არ ყავთ', 1000),
(5, 'ბაკურიანი', 14000, 'ბაკურიანის ბაკურიანი', 1784);

-- --------------------------------------------------------

--
-- Table structure for table `computers`
--

CREATE TABLE `computers` (
  `ID` int(11) NOT NULL,
  `პროცესორი` varchar(50) NOT NULL,
  `ვიდეობარათი` varchar(50) NOT NULL,
  `ოპერატიული მეხსიერება` int(50) NOT NULL,
  `მიხსიერების ტიპი` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `computers`
--

INSERT INTO `computers` (`ID`, `პროცესორი`, `ვიდეობარათი`, `ოპერატიული მეხსიერება`, `მიხსიერების ტიპი`) VALUES
(1, 'i7-7700K', 'RTX 3050 TI', 16, 'SSD'),
(2, 'i5-12600K', 'RTX 2060 SUPER', 32, 'SSD'),
(3, 'i9-9900K', 'RTX 4090 TI', 32, 'SSD'),
(4, 'i7-12700K', 'RTX 2060 SUPER', 16, 'SSD'),
(5, 'i5-8400', 'GTX 1080 TI', 16, 'SSD');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `ID` int(11) NOT NULL,
  `დასახელება` varchar(50) NOT NULL,
  `სტუდენტების რაოდენობა` int(50) NOT NULL,
  `რეიტინგი` double(50,5) NOT NULL,
  `წელი` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`ID`, `დასახელება`, `სტუდენტების რაოდენობა`, `რეიტინგი`, `წელი`) VALUES
(1, 'Hogwarts', 1500, 10.00000, 1930),
(2, 'ევროპის უნივერსიტეტი', 10200, 10.00000, 2015),
(3, 'კავკასიის უნივერსიტეტი', 9212, 5.00000, 1994),
(4, 'სკსუ', 200, 10.00000, 1992),
(5, 'ივერია პირველია', 14, 10.00000, 1980);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `computers`
--
ALTER TABLE `computers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `computers`
--
ALTER TABLE `computers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
